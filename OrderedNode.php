<?php
declare(strict_types=1);

namespace Laudis\Nodes;

use Laudis\Nodes\Databags\BasicContent;
use Youngsource\LaudisCommon\Contracts\Base\ContentInterface;
use Youngsource\LaudisCommon\Contracts\Base\TreeNodeInterface;
use Youngsource\LaudisCommon\Contracts\SubstructureIteratorInterface;
use Youngsource\LaudisCommon\Iterators\SimpleDepthFirstIterator;

/**
 * Class Node
 * @package Laudis\Nodes
 */
final class OrderedNode implements TreeNodeInterface
{
    /** @var string */
    private $slug;
    /** @var string */
    private $content;
    /** @var TreeNodeInterface|null */
    private $parent;
    /** @var OrderedNode[] */
    private $children = [];
    /** @var string */
    private $label;
    /** @var string */
    private $title;
    /** @var int */
    private $id;
    /** @var int|null */
    private $previousId;

    /**
     * Node constructor.
     * @param string $slug
     * @param string $content
     * @param string $label
     * @param string $title
     * @param int $id
     * @param int|null $previousId
     */
    public function __construct(
        string $slug,
        string $content,
        string $label,
        string $title,
        int $id,
        ?int $previousId
    ) {
        $this->slug = $slug;
        $this->content = $content;
        $this->label = $label;
        $this->title = $title;
        $this->id = $id;
        $this->previousId = $previousId;
    }

    /**
     * @param OrderedNode $child
     */
    public function addChild(OrderedNode $child): void
    {
        $this->children[$child->previousId] = $child;
    }

    /**
     * @return null|TreeNodeInterface
     */
    public function getParent(): ?TreeNodeInterface
    {
        return $this->parent;
    }

    /**
     * @param TreeNodeInterface $parent
     */
    public function setParent(TreeNodeInterface $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * Returns either the children of this content item
     * @return array
     */
    public function getChildren(): array
    {
        $current = $this->children[null] ?? null;
        $tbr = [];
        while ($current !== null) {
            $tbr[] = $current;
            $current = $this->children[$current->id] ?? null;
        }
        return $tbr;
    }

    /**
     * Returns the slug of this content item.
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * Returns the domain parameter.
     *
     * @return string
     */
    public function getDomainParameter(): string
    {
        return 'bundle';
    }

    /**
     * Returns the resource parameter.
     *
     * @return string
     */
    public function getResourceParameter(): string
    {
        return $this->label;
    }

    /**
     * @return array
     */
    public function getMetaInfo(): array
    {
        return [];
    }

    /**
     * Returns the Content.
     *
     * @return ContentInterface
     */
    public function content(): ContentInterface
    {
        return new BasicContent($this->title, $this->content);
    }

    /**
     * @return SubstructureIteratorInterface
     */
    public function getIterator(): SubstructureIteratorInterface
    {
        return new SimpleDepthFirstIterator($this);
    }
}
