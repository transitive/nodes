<?php
declare(strict_types=1);

namespace Laudis\Nodes;

use GraphAware\Neo4j\Client\Client;
use GraphAware\Neo4j\Client\ClientBuilder;
use Laudis\Common\Contracts\MutableContainerInterface;
use Psr\Container\ContainerInterface;

/**
 * Class NodesRegister
 * @package Laudis\Nodes
 */
final class NodesRegister
{
    /**
     * @param MutableContainerInterface $container
     */
    public function register(MutableContainerInterface $container): void
    {
        $container->set(Client::class, static function () {
            return ClientBuilder::create()
                ->addConnection('bolt', 'bolt://neo4j:rottemetotte@localhost:17687')
                ->build();
        });
        $container->set(NodeManager::class, static function (ContainerInterface $container) {
            return new NodeManager(
                $container->get(Client::class),
                new Neo4JAttributeGenerator($container->get(Client::class))
            );
        });
    }
}
