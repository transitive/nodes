<?php
declare(strict_types=1);

namespace Laudis\Nodes\Contracts;

/**
 * Interface NodeInterface
 * @package Laudis\Nodes
 */
interface NodeInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @param int $id
     */
    public function setId(int $id): void;

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void;

    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * Returns the neo4j database tag of the node.
     * @return string
     */
    public function getNodeTag(): string;
}
