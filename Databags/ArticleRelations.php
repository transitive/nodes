<?php
declare(strict_types=1);

namespace Laudis\Nodes\Databags;

/**
 * Class ArticleRelations
 * @package Laudis\Nodes
 */
final class ArticleRelations
{
    /**
     * @var Article
     */
    private $article;
    /**
     * @var Content
     */
    private $content;
    /**
     * @var Bundle
     */
    private $bundle;

    /**
     * ArticleRelations constructor.
     * @param Article $article
     * @param Content $content
     * @param Bundle $bundle
     */
    public function __construct(Article $article, Content $content, Bundle $bundle)
    {
        $this->article = $article;
        $this->content = $content;
        $this->bundle = $bundle;
    }

    /**
     * @return Article
     */
    public function getArticle(): Article
    {
        return $this->article;
    }

    /**
     * @return Content
     */
    public function getContent(): Content
    {
        return $this->content;
    }

    /**
     * @return Bundle
     */
    public function getBundle(): Bundle
    {
        return $this->bundle;
    }
}
