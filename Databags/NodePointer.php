<?php
declare(strict_types=1);

namespace Laudis\Nodes\Databags;

use Laudis\Nodes\Contracts\NodeInterface;

/**
 * Class ArticlePointer
 * @package Laudis\Nodes
 */
final class NodePointer
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var NodeInterface
     */
    private $current;
    /**
     * @var  NodePointer|null
     */
    private $parent;
    /**
     * @var NodePointer|null
     */
    private $previousSibling;

    /**
     * ArticlePointer constructor.
     * @param int|null $id
     * @param NodeInterface $current
     * @param NodePointer|null $parent
     * @param NodePointer|null $previousSibling
     */
    public function __construct(
        NodeInterface $current,
        ?NodePointer $parent = null,
        ?NodePointer $previousSibling = null,
        ?int $id = null
    ) {

        $this->id = $id;
        $this->current = $current;
        $this->parent = $parent;
        $this->previousSibling = $previousSibling;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @internal
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return NodeInterface
     */
    public function getCurrent(): NodeInterface
    {
        return $this->current;
    }

    /**
     * @return NodeInterface|null
     */
    public function getParent(): ?NodePointer
    {
        return $this->parent;
    }

    /**
     * @return NodeInterface|null
     */
    public function getPreviousSibling(): ?NodePointer
    {
        return $this->previousSibling;
    }
}
