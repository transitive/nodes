<?php
declare(strict_types=1);

namespace Laudis\Nodes\Databags;

use Laudis\Nodes\Contracts\NodeInterface;

/**
 * Class Article
 * @package Laudis\Nodes
 */
final class Article implements NodeInterface
{
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string|null
     */
    private $slug;

    /**
     * Article constructor.
     * @param string $title
     * @param int|null $id
     * @param string|null $slug
     */
    public function __construct(string $title, ?int $id = null, ?string $slug = null) {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @return string
     */
    public function getNodeTag(): string
    {
        return 'Article';
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug =  $slug;
    }
}
