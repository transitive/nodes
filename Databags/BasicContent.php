<?php
declare(strict_types=1);

namespace Laudis\Nodes\Databags;

use Youngsource\LaudisCommon\Contracts\Base\ContentInterface;

/**
 * Class BundleContent
 *
 * @package Youngsource\LaudisCommon\Content
 */
final class BasicContent implements ContentInterface
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $content;

    /**
     * NodeContent constructor.
     * @param string $title
     * @param string $content
     * ring $content
     */
    public function __construct(string $title, string $content)
    {
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getRawContent(): string
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getRawTitle(): string
    {
        return $this->title;
    }

}

