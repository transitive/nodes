<?php
declare(strict_types=1);

namespace Laudis\Nodes\Databags;

/**
 * Class ContentNode
 * @package Laudis\Nodes
 */
final class Content
{
    /**
     * @var string|null
     */
    private $contents;

    /**
     * ContentNode constructor.
     * @param string|null $contents
     */
    public function __construct(?string $contents)
    {
        $this->contents = $contents;
    }

    /**
     * @return string|null
     */
    public function getContents(): ?string
    {
        return $this->contents;
    }
}
