<?php
declare(strict_types=1);

namespace Laudis\Nodes\Databags;

use Laudis\Nodes\Contracts\NodeInterface;

/**
 * Class Bundle
 * @package Laudis\Nodes
 */
final class Bundle implements NodeInterface
{
    /**
     * @var int|null
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string|null
     */
    private $slug;

    /**
     * Bundle constructor.
     * @param string $title
     * @param int|null $id
     * @param string|null $slug
     */
    public function __construct(string $title, ?int $id = null, ?string $slug = null) {
        $this->title = $title;
        $this->id = $id;
        $this->slug = $slug;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * Returns the neo4j database tag of the node.
     * @return string
     */
    public function getNodeTag(): string
    {
        return 'Bundle';
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }
}
