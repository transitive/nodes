<?php
declare(strict_types=1);

namespace Laudis\Nodes;

use DateTimeInterface;
use Exception;
use GraphAware\Bolt\Record\RecordView;
use GraphAware\Neo4j\Client\ClientInterface as Client;
use GraphAware\Neo4j\Client\Exception\Neo4jException;
use GraphAware\Neo4j\Client\Stack;
use Laudis\Nodes\Contracts\NodeInterface;
use Laudis\Nodes\Databags\Article;
use Laudis\Nodes\Databags\Bundle;
use Laudis\Nodes\Databags\Content;
use Laudis\Nodes\Databags\NodePointer;
use Youngsource\LaudisCommon\Contracts\Base\TreeNodeInterface;
use function array_head;
use function array_last;
use function count;
use function str_replace;

/**
 * Class BundleManager
 * @package Laudis\Nodes
 */
final class NodeManager
{
    /** @var string */
    private const CONNECT_VALUE_QUERY = '
        MERGE (identifier:NodeIdentifier {id: {currentId}})
        ON CREATE SET identifier.createdAt = timestamp()
        ON MATCH SET identifier.updatedAt = timestamp()
        MERGE (current:NodePointer {addedSince: {addedSince}}) - [:IdentifiedBy] -> (identifier)
        ON CREATE SET current.createdAt = timestamp()
        ON MATCH SET current.updatedAt = timestamp()
        SET current.removedSince = {removedSince}
        WITH current
        MATCH (v:{valueLabel} {id: {valueId}})
        WITH current, v
        MERGE (current) <- [ref:Reference] -> (v)
    ';
    /** @var string */
    private const PARENT_QUERY = '
        WITH current
        MATCH (parent:NodeIdentifier {id: {parentId}})
        WITH parent, current
        MERGE (current) - [:Parent] -> (parent)
    ';
    /** @var string */
    private const PREVIOUS_SIBLING_QUERY = '
        WITH current
        MATCH (prev:NodeIdentifier {id: {previousId}})
        WITH prev, current
        MERGE (prev) <- [:PreviousSibling] - (current)
    ';
    /** @var int */
    public const MODE_INSTANT = 0x1;
    /** @var int */
    public const MODE_DEFERRED = 0x3;
    /**
     * @var Client
     */
    private $client;
    /**
     * @var Neo4JAttributeGenerator
     */
    private $generator;
    /** @var int */
    private $mode;
    /**
     * @var Stack
     */
    private $stack;
    /**
     * @var Bundle|null
     */
    private $bundle;

    /**
     * BundleManager constructor.
     * @param Client $client
     * @param Neo4JAttributeGenerator $slugGenerator
     * @param int $mode
     */
    public function __construct(Client $client, Neo4JAttributeGenerator $slugGenerator, int $mode = self::MODE_INSTANT)
    {
        $this->client = $client;
        $this->generator = $slugGenerator;
        $this->mode = $mode;
        $this->stack = $this->client->stack();
    }

    /**
     * @param Bundle $bundle
     */
    public function setBundle(?Bundle $bundle): void
    {
        $this->bundle = $bundle;
    }

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     */
    public function setMode(int $mode): void
    {
        $this->mode = $mode;
    }

    /**
     * @param NodeInterface $node
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface $removedSince
     * @return Bundle
     * @throws Exception
     */
    public function mergeVersionedNode(
        NodeInterface $node,
        DateTimeInterface $addedSince,
        DateTimeInterface $removedSince
    ): NodeInterface {
        $this->setIdAndSlug($node, $node->getNodeTag(), $node->getTitle());
        $this->basicContextualInsertion($node, $addedSince, $removedSince);
        return $node;
    }

    /**
     * @param NodePointer $pointer
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface $removedSince
     * @return NodePointer
     * @throws Exception
     */
    public function connect(
        NodePointer $pointer,
        DateTimeInterface $addedSince,
        DateTimeInterface $removedSince
    ): NodePointer {
        if ($pointer->getId() === null) {
            $pointer->setId($this->generator->generateId('NodeIdentifier'));
        }

        $query = self::CONNECT_VALUE_QUERY;
        $parentPtr = $pointer->getParent();
        $previousPtr = $pointer->getPreviousSibling();

        if ($parentPtr !== null) {
            $query .= self::PARENT_QUERY;
        }
        if ($previousPtr !== null) {
            $query .= self::PREVIOUS_SIBLING_QUERY;
        }
        $query = str_replace('{valueLabel}', $pointer->getCurrent()->getNodeTag(), $query);
        $params = [
            'currentId' => $pointer->getId(),
            'addedSince' => $addedSince->format('Y-m-d'),
            'removedSince' => $removedSince->format('Y-m-d'),
            'parentId' => $parentPtr !== null ? $parentPtr->getId() : null,
            'previousId' => $previousPtr !== null ? $previousPtr->getId() : null,
            'valueId' => $pointer->getCurrent()->getId()
        ];
        $this->pushOrRun($query, $params);
        return $pointer;
    }

    /**
     * @param Article $article
     * @param Content $contentNode
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface $removedSince
     */
    public function attachContent(
        Article $article,
        Content $contentNode,
        DateTimeInterface $addedSince,
        DateTimeInterface $removedSince
    ): void {
        $query = '
            CREATE (c:Content {content: {content}})
            ON CREATE SET b.createdAt = timestamp()
            MATCH (a:Article {id: {aid}})
            MERGE (c) <- [e:Content {addedSince: {addedSince}}] -> (a)
            ON CREATE SET e.createdAt = timestamp()
            ON MATCH SET e.updatedAt = timestamp()
            SET e.removedSince = {removedSince}
        ';
        $params = [
            'aid' => $article->getId(),
            'content' => $contentNode->getContents(),
            'addedSince' => $addedSince->format('Y-m-d'),
            'removedSince' => $removedSince->format('Y-m-d')
        ];
        $this->pushOrRun($query, $params);
    }

    /**
     * @throws Neo4jException
     */
    public function runStack(): void
    {
        $this->client->runStack($this->stack);
        $this->stack = $this->client->stack();
    }

    /**
     * @param Bundle $bundle
     * @param DateTimeInterface $context
     * @return TreeNodeInterface
     */
    public function tableOfContents(Bundle $bundle, DateTimeInterface $context): TreeNodeInterface
    {
        $records = $this->client->run('
MATCH (bundleId:NodeIdentifier {id: {id}}) <- [:IdentifiedBy] - (bundlePtr:NodePointer) - [:Reference] -> (:Bundle) - [:ContainsNode] -> (value:Article) <- [:Reference] - (ptr:NodePointer) - [:IdentifiedBy] -> (id:NodeIdentifier)

WHERE 	ptr.addedSince <= {context} AND ptr.removedSince > {context}
WITH 	collect(ptr) as ptrs, id.id as id, value
WITH 	ptrs, [ptr in ptrs | ptr.addedSince] as dates, id, value
UNWIND dates as date
WITH ptrs, max(date) as date , id, value
WITH [ ptr in ptrs WHERE ptr.addedSince = date | ptr ][0] as ptr, id, value

OPTIONAL MATCH (ptr) - [:Parent] -> (parent:NodeIdentifier)

WITH ptr, id, parent.id as parent, value

OPTIONAL MATCH (ptr) - [:PreviousSibling] -> (previousSibling:NodeIdentifier)

WITH ptr, id, parent, previousSibling.id as previousSibling, value

MATCH (value) - [version:ConsistsOfArticleVersions] -> ()

WHERE 	version.addedSince <= {context} AND version.removedSince > {context}
WITH 	collect(version) as ptrs, id, parent, previousSibling, value
WITH 	ptrs, [ptr in ptrs | ptr.addedSince] as dates, id, parent, previousSibling, value
UNWIND dates as date
WITH 	ptrs, max(date) as date, id, parent, previousSibling, value
WITH 	[ ptr in ptrs WHERE ptr.addedSince = date | ptr ][0] as version, id, parent, previousSibling, value

RETURN id, value.slug as slug, parent, endNode(version).title as title
        ', [
            'id' => $bundle->getId(),
            'context' => $context->format('Y-m-d')
        ])->records();

        $mappings = $this->createMapping($records, $bundle);
        $this->attachRelations($records, $mappings);

        return $mappings[$bundle->getId()];
    }

    /**
     * @param Bundle $bundle
     * @param int[] $positions
     * @param DateTimeInterface $context
     * @return NodeInterface
     */
    public function getNode(Bundle $bundle, array $positions, DateTimeInterface $context): NodeInterface
    {
        $node = $this->tableOfContents($bundle, $context);
        while (count($positions) !== 0) {
            $children = $node->getChildren();
            $position = array_head($positions);
            if (count($children) > $position) {
                $node = $children[$position];
            } else {
                /** @noinspection PhpParamsInspection */
                $node = array_last($children);
            }
        }
        return $node;
    }

    /**
     * @param NodeInterface $node
     * @param string $slugTag
     * @param string $slugSource
     * @return void
     * @throws Exception
     */
    private function setIdAndSlug(NodeInterface $node, string $slugTag, string $slugSource): void
    {
        if ($node->getId() === null) {
            $node->setId($this->generator->generateId($slugTag));
            $node->setSlug($this->generator->generateSlug($slugTag, $slugSource));
        }
    }

    /**
     * @param NodeInterface $node
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface $removedSince
     */
    private function basicContextualInsertion(
        NodeInterface $node,
        DateTimeInterface $addedSince,
        DateTimeInterface $removedSince
    ): void {
        $nodeLabel = $node->getNodeTag();
        $nodeVersionLabel = $nodeLabel . 'Version';
        $connectionLabel = 'ConsistsOf' . $nodeLabel . 'Versions';
        $query = "
            MERGE (b:$nodeLabel {slug: {slug}, id: {id}})
            ON CREATE SET b.createdAt = timestamp()
            ON MATCH SET b.updatedAt = timestamp()
            MERGE (bv:$nodeVersionLabel {title: {title}})
            ON CREATE SET bv.createdAt = timestamp()
            ON MATCH SET bv.updatedAt = timestamp() 
            MERGE (b) - [v:$connectionLabel {addedSince: {addedSince}}] -> (bv)
            ON CREATE SET v.createdAt = timestamp()
            ON MATCH SET v.updatedAt = timestamp()
            SET v.removedSince = {removedSince}
        ";
        $params = [
            'title' => $node->getTitle(),
            'id' => $node->getId(),
            'slug' => $node->getSlug(),
            'addedSince' => $addedSince->format('Y-m-d'),
            'removedSince' => $removedSince->format('Y-m-d')
        ];
        $this->pushOrRun($query, $params);
        if ($this->bundle !== null && $nodeLabel !== 'Bundle') {
            $query = "
MATCH   (node:$nodeLabel {slug: {nodeSlug}}),
        (bundle:Bundle {slug: {bundleSlug}})
WITH node, bundle
MERGE (bundle) - [:ContainsNode] -> (node)
            ";
            $params = ['nodeSlug' => $node->getSlug(), 'bundleSlug' => $this->bundle->getSlug()];
            $this->pushOrRun($query, $params);
        }
    }

    /**
     * @param string $query
     * @param array $params
     */
    private function pushOrRun(string $query, array $params): void
    {
        if ($this->isDeferred()) {
            $this->stack->push($query, $params);
        } else {
            $this->client->run($query, $params);
        }
    }

    /**
     * @return bool
     */
    private function isDeferred(): bool
    {
        return ($this->mode & self::MODE_DEFERRED) === self::MODE_DEFERRED;
    }

    /**
     * @param RecordView[] $records
     * @param Bundle $bundle
     * @return OrderedNode[]
     */
    private function createMapping(array $records, Bundle $bundle): array
    {
        $mappings = [];
        $mappings[$bundle->getId()] = new OrderedNode($bundle->getSlug(), '', 'bundle', $bundle->getTitle(),
            $bundle->getId(), null);
        foreach ($records as $record) {
            $slug = (string)$record->get('slug');
            $title = (string)$record->get('title');
            $id = (int)$record->get('id');
            $previous = $record->get('previous_id');
            $previous = $previous === null ? null : ((int)$previous);
            $mappings[$record->get('id')] = new OrderedNode($slug, '', 'article', $title, $id, $previous);
        }
        return $mappings;
    }

    /**
     * @param OrderedNode[] $records
     * @param OrderedNode[] $mappings
     */
    private function attachRelations(array $records, array $mappings): void
    {
        foreach ($records as $record) {
            if ($record->get('parent') !== null) {
                $parent = $mappings[$record->get('parent')];
                $child = $mappings[$record->get('id')];
                $parent->addChild($child);
                $child->setParent($parent);
            }
        }
    }
}
