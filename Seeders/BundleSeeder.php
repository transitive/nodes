<?php
declare(strict_types=1);

namespace Laudis\Nodes\Seeders;

use DateTimeInterface;
use Exception;
use Faker\Generator;
use Laudis\Nodes\Contracts\NodeInterface;
use Laudis\Nodes\Databags\Article;
use Laudis\Nodes\Databags\Bundle;
use Laudis\Nodes\Databags\NodePointer;
use Laudis\Nodes\NodeManager;
use function array_merge;
use function array_slice;
use function count;
use function max_date;
use function min_date;
use function shuffle;
use function simple_date;

/**
 * Class BundleSeeder
 * @package Laudis\Nodes
 */
final class BundleSeeder
{
    /** @var NodeManager */
    private $manager;
    /** @var Generator */
    private $generator;

    /**
     * BundleSeeder constructor.
     * @param NodeManager $bundle
     * @param Generator $generator
     */
    public function __construct(NodeManager $bundle, Generator $generator)
    {
        $this->manager = $bundle;
        $this->generator = $generator;
    }

    /**
     * @throws Exception
     */
    public function seed(): void
    {
        $bundle = new Bundle('Test Bundel');
        $bundle = $this->manager->mergeVersionedNode($bundle, min_date(), max_date());
        $bundle = new Bundle('Test Bundle - Revised', $bundle->getId(), $bundle->getSlug());
        $this->manager->mergeVersionedNode($bundle, simple_date(2010, 1, 1), max_date());

        $this->manager->setBundle($bundle);

        $articles = [];
        for ($i = 1, $iMax = 5 ** 4; $i < $iMax; ++$i) {
            $articles[] = $this->generateArticle();
        }

        $pointers = [$this->manager->connect(new NodePointer($bundle), min_date(), max_date())];
        $items = array_merge([$bundle], $articles);
        $pointers = $this->connectPointersForPeriod($items, $pointers, min_date(), max_date());
        $this->reshufflePointersForPeriod($pointers, simple_date(2005, 1, 1), max_date());
        $this->reshufflePointersForPeriod($pointers, simple_date(2010, 1, 1), max_date());
    }

    /**
     * @return Article|Bundle|NodeInterface
     * @throws Exception
     */
    private function generateArticle()
    {
        $article = new Article($this->randomTitle());
        $article = $this->manager->mergeVersionedNode($article, min_date(), max_date());
        $article = new Article($this->randomTitle(), $article->getId(), $article->getSlug());
        $this->manager->mergeVersionedNode($article, simple_date(2005, 1, 1), max_date());
        $article = new Article($this->randomTitle(), $article->getId(), $article->getSlug());
        $this->manager->mergeVersionedNode($article, simple_date(2010, 1, 1), max_date());
        return $article;
    }

    /**
     * @return array|string
     */
    private function randomTitle()
    {
        return $this->generator->words($this->generator->numberBetween(1, 10), true);
    }

    /**
     * @param array $items
     * @param array $pointers
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface $removedSince
     * @return array
     * @throws Exception
     */
    private function connectPointersForPeriod(
        array $items,
        array $pointers,
        DateTimeInterface $addedSince,
        DateTimeInterface $removedSince
    ): array {
        for ($i = 1, $iMax = count($items); $i < $iMax; ++$i) {
            // Use 5-ary tree arithmetic
            $parent = $pointers[(int) (($i - 1) / 5)];
            $current = $items[$i];
            if ($i === 1) {
                $previous = null;
            } else {
                $previous = ((int)(($i - 2) / 5) === (int)(($i - 1) / 5)) ? $pointers[$i - 1] : null;
            }

            $pointer = new NodePointer($current, $parent, $previous, null);
            $pointer = $this->manager->connect($pointer, $addedSince, $removedSince);
            $pointers[] = $pointer;
        }
        return $pointers;
    }

    /**
     * @param NodePointer[] $pointers
     * @param DateTimeInterface $addedSince
     * @param DateTimeInterface $removedSince
     * @return array
     * @throws Exception
     */
    private function reshufflePointersForPeriod(
        array $pointers,
        DateTimeInterface $addedSince,
        DateTimeInterface $removedSince
    ): array {
        $articlePtrs = array_slice($pointers, 1);
        /** @noinspection NonSecureShuffleUsageInspection */
        shuffle($articlePtrs);
        $pointers = array_merge([$pointers[0]], $articlePtrs);
        for ($i = 1, $iMax = count($pointers); $i < $iMax; ++$i) {
            // Use 5-ary tree arithmetic
            $parent = $pointers[(int) (($i - 1) / 5)];
            $current = $pointers[$i];
            if ($i === 1) {
                $previous = null;
            } else {
                $previous = ((int)(($i - 2) / 5) === (int)(($i - 1) / 5)) ? $pointers[$i - 1] : null;
            }

            $pointer = new NodePointer($current->getCurrent(), $parent, $previous, $current->getId());
            $pointer = $this->manager->connect($pointer, $addedSince, $removedSince);
            $pointers[] = $pointer;
        }
        return $pointers;
    }
}
