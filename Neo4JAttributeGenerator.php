<?php
declare(strict_types=1);

namespace Laudis\Nodes;

use function array_last;
use function explode;
use GraphAware\Neo4j\Client\ClientInterface;
use RuntimeException;
use function str_replace;
use function strtolower;

/**
 * Class Neo4JSlugGenerator
 * @package Laudis\Nodes
 */
final class Neo4JAttributeGenerator
{
    /** @var ClientInterface */
    private $client;

    /**
     * Neo4JSlugGenerator constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $tag
     * @param string $value
     * @return string
     */
    public function generateSlug(string $tag, string $value): string
    {
        $slug = strtolower(str_replace(' ', '-', $value));
        $query = "MATCH (x:$tag) WHERE x.slug =~ {slug} RETURN x.slug AS slug ORDER BY slug DESC";
        $result = $this->client->run($query, ['slug' => $slug . '(-\d+)?']);

        if ($result === null) {
            throw $this->queryException($query);
        }

        $record = $result->firstRecordOrDefault(null);
        $otherSlug = $record === null ? null : $record->get('slug');
        if ($otherSlug !== null) {
            $slug .= '-' . ((int)array_last(explode('-', $otherSlug)) + 1);
        }

        return $slug;
    }

    /**
     * @param string $tag
     * @return int
     */
    public function generateId(string $tag): int
    {
        $query = "MATCH (x:$tag) RETURN max(x.id) as id";
        $result = $this->client->run($query);

        if ($result === null) {
            $this->queryException($query);
        }

        $record = $result->firstRecordOrDefault(null);
        $id = $record === null ? null : $record->get('id');
        if ($id !== null) {
            return ((int) $id) + 1;
        }

        return 1;
    }

    /**
     * @param string $query
     * @return RuntimeException
     * @return RuntimeException
     */
    private function queryException(string $query): RuntimeException
    {
        return new RuntimeException("The result of the query: [[ $query ]] did not return any result");
    }
}
